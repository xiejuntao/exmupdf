/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.artifex.mupdfdemo;

public final class R {
    public static final class animator {
        public static final int info=0x7f040000;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int black=0x7f050010;
        public static final int busy_indicator=0x7f050003;
        public static final int button_normal=0x7f050004;
        public static final int button_pressed=0x7f050005;
        public static final int canvas=0x7f050000;
        public static final int list_view_divider=0x7f05000f;
        public static final int page_indicator=0x7f050002;
        public static final int seek_progress=0x7f05000c;
        public static final int seek_thumb=0x7f05000b;
        public static final int text_border_focused=0x7f05000a;
        public static final int text_border_normal=0x7f050008;
        public static final int text_border_pressed=0x7f050009;
        public static final int text_normal=0x7f050006;
        public static final int text_pressed=0x7f050007;
        public static final int toolbar=0x7f050001;
        public static final int transparent=0x7f05000e;
        public static final int white=0x7f05000d;
    }
    public static final class dimen {
        /**  气泡面板按钮字体大小 
         */
        public static final int pop_button_fontsize=0x7f060016;
        /**  气泡面板按钮高度 
         */
        public static final int pop_button_height=0x7f060014;
        /**  气泡面板按钮内字体左右间距 
         */
        public static final int pop_button_padding=0x7f060017;
        /**  气泡面板按钮宽度 
         */
        public static final int pop_button_width=0x7f060015;
        /**  气泡面板项的字体大小 
         */
        public static final int pop_item_fontsize=0x7f060018;
        /**  气泡面板项的间距 
         */
        public static final int pop_item_padding=0x7f060019;
        /**  气泡面板上下间距 
         */
        public static final int pop_padding=0x7f060011;
        /**  气泡面板标题字体大小 
         */
        public static final int pop_title_fontsize=0x7f060013;
        /**  气泡面板标题高度 
         */
        public static final int pop_title_height=0x7f060012;
        /**  气泡面板宽度 
         */
        public static final int pop_width=0x7f060010;
        public static final int sp_12=0x7f060000;
        public static final int sp_13=0x7f060001;
        public static final int sp_14=0x7f060002;
        public static final int sp_15=0x7f060003;
        public static final int sp_16=0x7f060004;
        public static final int sp_17=0x7f060005;
        public static final int sp_18=0x7f060006;
        public static final int sp_19=0x7f060007;
        public static final int sp_20=0x7f060008;
        public static final int sp_21=0x7f060009;
        public static final int sp_22=0x7f06000a;
        public static final int sp_23=0x7f06000b;
        /**  正文编辑工具条按钮高度 
         */
        public static final int text_edit_bar_height=0x7f06000c;
        /**  正文编辑工具条按钮宽度 
         */
        public static final int text_edit_bar_width=0x7f06000d;
        /**  正文编辑工具条按钮最小间隔 
         */
        public static final int text_min_width=0x7f06000e;
        public static final int text_normal_width=0x7f06000f;
    }
    public static final class drawable {
        public static final int btn_normal=0x7f020000;
        public static final int busy=0x7f020001;
        public static final int button=0x7f020002;
        public static final int comment_bg=0x7f020003;
        public static final int darkdenim3=0x7f020004;
        public static final int ic_annot=0x7f020005;
        public static final int ic_annotation=0x7f020006;
        public static final int ic_arrow_left=0x7f020007;
        public static final int ic_arrow_right=0x7f020008;
        public static final int ic_arrow_up=0x7f020009;
        public static final int ic_cancel=0x7f02000a;
        public static final int ic_check=0x7f02000b;
        public static final int ic_clipboard=0x7f02000c;
        public static final int ic_dir=0x7f02000d;
        public static final int ic_doc=0x7f02000e;
        public static final int ic_highlight=0x7f02000f;
        public static final int ic_launcher=0x7f020010;
        public static final int ic_link=0x7f020011;
        public static final int ic_list=0x7f020012;
        public static final int ic_magnifying_glass=0x7f020013;
        public static final int ic_more=0x7f020014;
        public static final int ic_pen=0x7f020015;
        public static final int ic_print=0x7f020016;
        public static final int ic_reflow=0x7f020017;
        public static final int ic_select=0x7f020018;
        public static final int ic_strike=0x7f020019;
        public static final int ic_trash=0x7f02001a;
        public static final int ic_underline=0x7f02001b;
        public static final int ic_updir=0x7f02001c;
        public static final int icon=0x7f02001d;
        public static final int page_num=0x7f02001e;
        public static final int pop_bg=0x7f02001f;
        public static final int rounded_corners_pop=0x7f020020;
        public static final int rounded_corners_view=0x7f020021;
        public static final int search=0x7f020022;
        public static final int seek_progress=0x7f020023;
        public static final int seek_thumb=0x7f020024;
        public static final int tiled_background=0x7f020025;
    }
    public static final class id {
        public static final int RelativeLayout01=0x7f09001e;
        public static final int acceptButton=0x7f09000b;
        public static final int cancelSearch=0x7f09000d;
        public static final int colorButton=0x7f090006;
        public static final int colorPickerView=0x7f090015;
        public static final int comentEditTime=0x7f090024;
        public static final int comentId=0x7f090020;
        public static final int commentActName=0x7f090023;
        public static final int commentBtn=0x7f090005;
        public static final int commentCheckBox=0x7f09001f;
        public static final int commentEditable=0x7f090021;
        public static final int commentEditor=0x7f090022;
        public static final int commentList=0x7f09001d;
        public static final int commentText=0x7f09001b;
        public static final int docNameText=0x7f090002;
        public static final int emitButton=0x7f090004;
        public static final int eraserButton=0x7f090009;
        public static final int fontSeekBar=0x7f090017;
        public static final int fontText=0x7f090016;
        public static final int icon=0x7f090027;
        public static final int info=0x7f090014;
        public static final int inkButton=0x7f09000a;
        public static final int layout_root=0x7f090018;
        public static final int layout_sendTarget=0x7f090019;
        public static final int layout_title=0x7f09001a;
        public static final int linkButton=0x7f090003;
        public static final int lowerButtons=0x7f090011;
        public static final int name=0x7f090028;
        public static final int outlineButton=0x7f090007;
        public static final int page=0x7f090026;
        public static final int pageNumber=0x7f090013;
        public static final int pageSlider=0x7f090012;
        public static final int searchBack=0x7f09000f;
        public static final int searchButton=0x7f090008;
        public static final int searchForward=0x7f090010;
        public static final int searchText=0x7f09000e;
        public static final int selButton=0x7f09001c;
        public static final int switcher=0x7f090000;
        public static final int title=0x7f090025;
        public static final int topBar0Main=0x7f090001;
        public static final int topBar1Search=0x7f09000c;
    }
    public static final class layout {
        public static final int buttons=0x7f030000;
        public static final int draw_color=0x7f030001;
        public static final int draw_comment=0x7f030002;
        public static final int draw_comment_list=0x7f030003;
        public static final int main=0x7f030004;
        public static final int outline_entry=0x7f030005;
        public static final int picker_entry=0x7f030006;
        public static final int textentry=0x7f030007;
    }
    public static final class string {
        public static final int accept=0x7f070001;
        public static final int app_name=0x7f070002;
        public static final int cancel=0x7f070003;
        public static final int cannot_open_buffer=0x7f070004;
        public static final int cannot_open_document=0x7f070005;
        public static final int cannot_open_document_Reason=0x7f070006;
        public static final int cannot_open_file_Path=0x7f070007;
        public static final int choose_value=0x7f070008;
        public static final int color=0x7f070017;
        public static final int comment=0x7f070018;
        public static final int copied_to_clipboard=0x7f070009;
        public static final int copy=0x7f07000a;
        public static final int copy_text=0x7f07000b;
        public static final int copy_text_to_the_clipboard=0x7f07000c;
        public static final int delete=0x7f07000d;
        public static final int dismiss=0x7f07000e;
        public static final int document_has_changes_save_them_=0x7f07000f;
        public static final int draw_annotation=0x7f070010;
        public static final int draw_comment_title=0x7f070037;
        public static final int edit_annotations=0x7f070011;
        public static final int enter_password=0x7f070012;
        public static final int entering_reflow_mode=0x7f070013;
        public static final int eraser_text=0x7f070036;
        public static final int fill_out_text_field=0x7f070014;
        public static final int format_currently_not_supported=0x7f070015;
        public static final int highlight=0x7f070016;
        public static final int ink=0x7f070019;
        public static final int leaving_reflow_mode=0x7f07001a;
        public static final int more=0x7f07001b;
        public static final int no=0x7f07001c;
        public static final int no_further_occurrences_found=0x7f07001d;
        public static final int no_media_hint=0x7f07001e;
        public static final int no_media_warning=0x7f07001f;
        public static final int no_text_selected=0x7f070020;
        public static final int not_supported=0x7f070021;
        public static final int nothing_to_save=0x7f070022;
        public static final int okay=0x7f070023;
        public static final int outline_title=0x7f070024;
        public static final int parent_directory=0x7f070025;
        public static final int picker_title_App_Ver_Dir=0x7f070026;
        public static final int print=0x7f070027;
        public static final int print_failed=0x7f070028;
        public static final int save=0x7f070029;
        public static final int search=0x7f07002a;
        public static final int search_backwards=0x7f07002b;
        public static final int search_document=0x7f07002c;
        public static final int search_forwards=0x7f07002d;
        public static final int searching_=0x7f07002e;
        public static final int select=0x7f07002f;
        public static final int select_text=0x7f070030;
        public static final int strike_out=0x7f070031;
        public static final int text_not_found=0x7f070032;
        public static final int toggle_links=0x7f070033;
        public static final int underline=0x7f070034;
        public static final int version=0x7f070000;
        public static final int yes=0x7f070035;
    }
    public static final class style {
        public static final int AppBaseTheme=0x7f080000;
    }
}
