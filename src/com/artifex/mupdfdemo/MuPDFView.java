package com.artifex.mupdfdemo;

import android.graphics.PointF;
import android.graphics.RectF;

enum Hit {Nothing, Widget, Annotation};

public interface MuPDFView {
	public void setPage(int page, PointF size);
	public void setScale(float scale);
	public int getPage();
	public void blank(int page);
	public LinkInfo hitLink(float x, float y);
	public void setSearchBoxes(RectF searchBoxes[]);
	public void setLinkHighlighting(boolean f);
	public void startDraw(float x, float y);
	public void continueDraw(float x, float y);
	public void cancelDraw();
	public void update();
	public void addHq(boolean update);
	public void removeHq();
	public void releaseResources();
	public Hit passClickEvent(float x, float y);
	public void eraser(float x,float y);
	public void refreshComment();
}
