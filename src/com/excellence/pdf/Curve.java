package com.excellence.pdf;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import android.graphics.Point;

public class Curve {
	/**
	 * 颜色
	 * */
	private String cl;
	/**
	 * 笔粗
	 * */
	private String tn;
	/**
	 * 画板宽度
	 * */
	private String fw;
	/**
	 * 画板高度
	 * */
	private String fh;
	/**
	 * 点集
	 * */
	private List<Point> ps = new ArrayList<Point>();
	private Element curveElement = null;
	private Element psElement = null;
	private Element pElement = null;
	private Document doc = null;
	public String getCl() {
		return cl;
	}
	public void setCl(String cl) {
		this.cl = cl;
	}
	public String getTn() {
		return tn;
	}
	public void setTn(String tn) {
		this.tn = tn;
	}
	public String getFw() {
		return fw;
	}
	public void setFw(String fw) {
		this.fw = fw;
	}
	public String getFh() {
		return fh;
	}
	public void setFh(String fh) {
		this.fh = fh;
	}
	public List<Point> getPs() {
		return ps;
	}
	public void setPs(List<Point> ps) {
		this.ps = ps;
	}
	public Element toElement(Document doc){
		this.doc = doc;
		curveElement = doc.createElement("curve");
		Element clElement = doc.createElement("cl");
		Element tnElement = doc.createElement("tn");
		clElement.setTextContent(this.getCl());
		tnElement.setTextContent(this.getTn());
		Element fwElement = doc.createElement("fw");
		fwElement.setTextContent(this.getFw());
		Element fhElement = doc.createElement("fh");
		fhElement.setTextContent(this.getFh());
		curveElement.appendChild(clElement);
		curveElement.appendChild(tnElement);
		curveElement.appendChild(fwElement);
		curveElement.appendChild(fhElement);
		psElement = doc.createElement("ps");
		curveElement.appendChild(psElement);
		return curveElement;
	}
	public void addPoint(int x,int y){
		pElement = doc.createElement("p");
		pElement.setTextContent(x+","+y);
		psElement.appendChild(pElement);
	}
}
