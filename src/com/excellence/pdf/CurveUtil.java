package com.excellence.pdf;

import android.graphics.Point;
import android.util.FloatMath;

public class CurveUtil {
	public static float eraserRadius = 20;
	public static boolean lineInterCircle(Point ptStart, Point ptEnd,
			Point ptCenter) {
		float fDis = FloatMath.sqrt((ptEnd.x - ptStart.x)
				* (ptEnd.x - ptStart.x) + (ptEnd.y - ptStart.y)
				* (ptEnd.y - ptStart.y));
		if (fDis == 0) {
			// 点重叠，则判断该点群到橡皮擦的距离是否大于圆的半径
			float d = FloatMath.sqrt((float) (Math.pow(
					Math.abs(ptStart.x - ptCenter.x), 2) + Math.pow(
					Math.abs(ptStart.y - ptCenter.y), 2)));
			return (d < eraserRadius) ? true : false;
		}
		Point dPoint = new Point();
		dPoint.x = (int) ((ptEnd.x - ptStart.x) / fDis);
		dPoint.y = (int) ((ptEnd.y - ptStart.y) / fDis);
		Point ePoint = new Point();
		ePoint.x = ptCenter.x - ptStart.x;
		ePoint.y = ptCenter.y - ptStart.y;
		float a = ePoint.x * dPoint.x + ePoint.y * dPoint.y;
		float a2 = a * a;
		float e2 = ePoint.x * ePoint.x + ePoint.y * ePoint.y;
		float r2 = eraserRadius * eraserRadius;
		if ((r2 - e2 + a2) < 0) {
			return false;
		} else {
			// 求交点
			float f = FloatMath.sqrt(r2 - e2 + a2);
			float t = a - f;
			if (((t - 0.0) > -0.00001) && (t - fDis) < 0.00001) {
				return true;
			}
			t = a + f;
			if (((t - 0.0) > -0.00001) && (t - fDis) < 0.00001) {
				return true;
			}
		}
		return false;
	}
	public static int getTh(String th){
		int t;
		try{
			t = Integer.parseInt(th);
		}catch(Exception e){
			t = 2;
		}
		return t;
	}
}
