package com.excellence.pdf;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

public class PaintUtil {
	private float mX, mY;
	private Path path;
	private Paint paint;
	private Canvas canvas;
	private static final float TOUCH_TOLERANCE = 4;
	public PaintUtil(Path path, Paint paint) {
		super();
		this.path = path;
		this.paint = paint;
	}
	public PaintUtil(Path path, Paint paint, Canvas canvas) {
		super();
		this.path = path;
		this.paint = paint;
		this.canvas = canvas;
	}

	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}

	public void touch_start(float x, float y) {
        path.reset();
        path.moveTo(x, y);
        mX = x;
        mY = y;
    }
    public void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            path.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
            mX = x;
            mY = y;
        }
    }
    public void touch_up() {
        path.lineTo(mX, mY);
        // commit the path to our offscreen
        canvas.drawPath(path,paint);
        // kill this so we don't double draw
        path.reset();
    }
	public void setColor(int color) {
		this.paint.setColor(color);
	}
	public void setStrokeWidth(int width) {
		this.paint.setStrokeWidth(width);
	}
}
