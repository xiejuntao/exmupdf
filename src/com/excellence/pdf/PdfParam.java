package com.excellence.pdf;

import java.util.ArrayList;
import java.util.List;

import com.excellence.pdf.comment.PdfComment;

import android.graphics.Color;

public class PdfParam {
	private int thickness = 2;
	private int color = Color.BLACK;
	private boolean editable = true;
	private List<PdfComment> comments = new ArrayList<PdfComment>();
	public PdfParam(){
		PdfComment pdfComment1 = new PdfComment();
		pdfComment1.setId("100");
		pdfComment1.setEditable(false);
		pdfComment1.setActName("步骤一");
		pdfComment1.setAuthor("批示人一");
		pdfComment1.setEditDate("2012-12-20");
		pdfComment1.setFilePath("/mnt/sdcard/.exoa/pdf/bb1");
		comments.add(pdfComment1);
		PdfComment pdfComment2 = new PdfComment();
		pdfComment2.setId("101");
		pdfComment2.setEditable(false);
		pdfComment2.setActName("步骤二");
		pdfComment2.setAuthor("批示人二");
		pdfComment2.setEditDate("2012-12-20");
		pdfComment2.setFilePath("/mnt/sdcard/.exoa/pdf/bb2");
		comments.add(pdfComment2);
		PdfComment pdfComment3 = new PdfComment();
		pdfComment3.setId("102");
		pdfComment3.setEditable(false);
		pdfComment3.setActName("步骤三");
		pdfComment3.setAuthor("批示人三");
		pdfComment3.setEditDate("2012-12-25");
		pdfComment3.setFilePath("/mnt/sdcard/.exoa/pdf/bb3");
		comments.add(pdfComment3);
		PdfComment pdfComment4 = new PdfComment();
		pdfComment4.setId("103");
		pdfComment4.setEditable(true);
		pdfComment4.setActName("步骤四");
		pdfComment4.setAuthor("当前批示人");
		pdfComment4.setEditDate("2013-12-25");
		pdfComment4.setFilePath("/mnt/sdcard/.exoa/pdf/bb4");
		comments.add(pdfComment4);
	}
	public int getThickness() {
		return thickness;
	}
	public void setThickness(int thickness) {
		this.thickness = thickness;
	}
	public int getColor() {
		return color;
	}
	public void setColor(int color) {
		this.color = color;
	}
	public List<PdfComment> getComments() {
		return comments;
	}
	public void setComments(List<PdfComment> comments) {
		this.comments = comments;
	}
	public boolean isEditable() {
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	public void mapping(List<String> commentIds){
		for(PdfComment comment:comments){
			boolean isSelected = false;
			for(String commentId:commentIds){
				if(commentId.equals(comment.getId())){
					comment.setCheck(true);
					isSelected = true;
					break;
				}
			}
			if(!isSelected){
				comment.setCheck(false);
			}
		}
	}
}
