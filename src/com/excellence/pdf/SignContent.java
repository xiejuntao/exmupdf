package com.excellence.pdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.excellence.pdf.color.ColorUtil;

import android.R.integer;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;


public class SignContent {
	/**
	 * 当前页，pdf签名才有此属性
	 * */
	private String page;
	/**
	 * 作者,pdf签名才有此属性
	 * */
	private String author;
	/**
	 * 曲线集
	 * */
	//private List<Curve> curves = new ArrayList<Curve>();
	/**
	 * 图章集，表单签名控件才有此属性
	 * */
	//private List<Seal> seals = new ArrayList<Seal>();
	private String id;
	private Document doc;
	private Element rootElement;
	private File file = null;
	private boolean check = true;
	public SignContent(File file)throws Exception{
		this.file = file;
		DocumentBuilderFactory domfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder dombuilder = domfac.newDocumentBuilder();
		InputStream is = new FileInputStream(file);
		doc = dombuilder.parse(is);
		rootElement = doc.getDocumentElement();
	}
	public SignContent(File file, String id,boolean check)throws Exception{
		this(file);
		this.id = id;
		this.check = check;
	}
	public SignContent(Document document,Element rootElement){
		this.doc = document;
		this.rootElement = rootElement; 
	}
	public void addCurve(Curve curve){
		rootElement.appendChild(curve.toElement(doc));
	}

	public boolean removeCurve(Point ptCenter,int curWidth){
		boolean match = false;
		if(rootElement==null){
			return false;
		}
		NodeList nodeList = rootElement.getElementsByTagName("curve");
		if(null!=nodeList && 0 < nodeList.getLength()){  
		    for(int i=0; i < nodeList.getLength(); i++){
		    	Element curve = (Element)nodeList.item(i);
		    	Element ps = (Element)curve.getElementsByTagName("ps").item(0);
				NodeList pNodeList = ps.getElementsByTagName("p");
				if(null==pNodeList||1>=pNodeList.getLength()){
					continue;
				}
				Element fw = (Element)curve.getElementsByTagName("fw").item(0);
				float scale = curWidth/Float.parseFloat(fw.getTextContent());
				String[] pointsStrings = pNodeList.item(0).getTextContent().split(",");
				float x = Integer.parseInt(pointsStrings[0]);
				float y = Integer.parseInt(pointsStrings[1]);
				Point ptStart = new Point();
				Point ptEnd = new Point();
				ptStart.x = (int)(x * scale);
				ptStart.y = (int)(y * scale);
				for(int j=1;j<pNodeList.getLength();j++){
					String[] pStrings = pNodeList.item(j).getTextContent().split(",");
					x = Integer.parseInt(pStrings[0]) * scale;
					y = Integer.parseInt(pStrings[1]) * scale;
					ptEnd.x = (int)x;
					ptEnd.y = (int)y;
					if(CurveUtil.lineInterCircle(ptStart, ptEnd, ptCenter)){
						curve.getParentNode().removeChild(curve);
						match = true;
						break;
					}
					ptStart.x = (int)x;
					ptStart.y = (int)y;
				}
		    }
		}
		return match;
	}
	public void removeSign(){
		if(rootElement==null){
			return;
		}
		NodeList nodeList = rootElement.getElementsByTagName("curve");
		if(null!=nodeList && 0 < nodeList.getLength()){
			for(int i=0; i < nodeList.getLength(); i++){	
		    	Element curve = (Element)nodeList.item(i);
		    	curve.getParentNode().removeChild(curve);
			}
		}
		NodeList sealNodeList = rootElement.getElementsByTagName("seal");
		if(null!=sealNodeList&&0<sealNodeList.getLength()){
			for(int i=0; i < sealNodeList.getLength(); i++){	
		    	Element seal = (Element)sealNodeList.item(i);
		    	seal.getParentNode().removeChild(seal);
			}
		}
	}

	public void save(){
		try {
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty("indent", "yes");
			transformer.transform(new DOMSource(this.doc), new StreamResult(new FileOutputStream(this.file)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public  void drawPdfCurve(PaintUtil paintUtil, int curWidth){
		if(rootElement!=null){
			NodeList nodeList = rootElement.getElementsByTagName("curve");
			if(nodeList!=null){
				int length = nodeList.getLength();
				for(int i=(length-1);i>-1; i--){
			    	Element curve = (Element)nodeList.item(i);
			    	Element cl = (Element)curve.getElementsByTagName("cl").item(0);
			    	paintUtil.setColor(ColorUtil.toColor(cl.getTextContent()));
					Element tn = (Element)curve.getElementsByTagName("tn").item(0);
					Element ps = (Element)curve.getElementsByTagName("ps").item(0);
					NodeList pNodeList = ps.getElementsByTagName("p");
					Element fw = (Element)curve.getElementsByTagName("fw").item(0);
					float tempScale = curWidth/Float.parseFloat(fw.getTextContent());
					paintUtil.setStrokeWidth((int)(Integer.parseInt(tn.getTextContent())*tempScale));
					int l = pNodeList.getLength();
					if(null==pNodeList||l<=2){
						continue;
					}
					String[] pointsStrings = pNodeList.item(0).getTextContent().split(",");
					float x = Integer.parseInt(pointsStrings[0]);
					float y = Integer.parseInt(pointsStrings[1]);
					paintUtil.touch_start(x * tempScale,y * tempScale);
					for(int j=1;j<l;j++){
						String[] pStrings = pNodeList.item(j).getTextContent().split(",");
						x = Integer.parseInt(pStrings[0]) * tempScale;
						y = Integer.parseInt(pStrings[1]) * tempScale;
						paintUtil.touch_move(x, y);
					}
					paintUtil.touch_up();
			    }
			}
		}
	}
	public void drawPdfCurve2(Canvas canvas,Paint paint,int curWidth){
		if(rootElement!=null){
			Path path = new Path();
			NodeList nodeList = rootElement.getElementsByTagName("curve");
			if(null!=nodeList && 0 < nodeList.getLength()){
				for(int i=0; i < nodeList.getLength(); i++){
					Element curve = (Element)nodeList.item(i);
			    	Element ps = (Element)curve.getElementsByTagName("ps").item(0);
					NodeList pNodeList = ps.getElementsByTagName("p");
					int l = pNodeList.getLength();
					if(l>=2){
						Element cl = (Element)curve.getElementsByTagName("cl").item(0);
						Element tn = (Element)curve.getElementsByTagName("tn").item(0);
						Element fw = (Element)curve.getElementsByTagName("fw").item(0);
						float scale = curWidth/Float.parseFloat(fw.getTextContent());
				    	paint.setStrokeWidth(CurveUtil.getTh(tn.getTextContent()) * scale);
						paint.setColor(ColorUtil.toColor(cl.getTextContent()));
						String[] pointsStrings = pNodeList.item(0).getTextContent().split(",");
						float mX = Integer.parseInt(pointsStrings[0]) * scale;
						float mY = Integer.parseInt(pointsStrings[1]) * scale;
						path.moveTo(mX, mY);
						for(int j=1;j<l;j++){
							pointsStrings = pNodeList.item(j).getTextContent().split(",");
							float x = Integer.parseInt(pointsStrings[0]) * scale;
							float y = Integer.parseInt(pointsStrings[1]) * scale;
							path.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
							mX = x;
							mY = y;
						}
						path.lineTo(mX, mY);
					}
				}
			}
			canvas.drawPath(path, paint);
		}
	}
	public void createIfNotExist(String curPage,String author){
		if(rootElement == null){
			rootElement = doc.createElement("s_sign_content");
			Element pageElement = doc.createElement("page");
			pageElement.setTextContent(String.valueOf(curPage));
			Element authorElement = doc.createElement("author");
			authorElement.setTextContent(author);
			rootElement.appendChild(pageElement);
			rootElement.appendChild(authorElement);
			doc.getDocumentElement().appendChild(rootElement);
		}
	}
	public boolean hasContent(){
		boolean hasContent = true;
		if(rootElement!=null){
			NodeList nodeList = rootElement.getElementsByTagName("curve");
			NodeList sealNodeList = rootElement.getElementsByTagName("seal");
			if((null!=nodeList && 0 < nodeList.getLength())||(null!=sealNodeList&&0<sealNodeList.getLength())){
				hasContent = true;
			}else{
				hasContent = false;
			}
		}
		return hasContent;
	}
	public void setRootElement(Element rootElement){
		this.rootElement = rootElement;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ArrayList<ArrayList<PointF>> getNodeList(){
		if(rootElement==null){
			return null;
		}
		ArrayList<ArrayList<PointF>> resultList = new ArrayList<ArrayList<PointF>>();
		NodeList nodeList = rootElement.getElementsByTagName("curve");
		if(null!=nodeList && 0 < nodeList.getLength()){
			for(int i=0; i < nodeList.getLength(); i++){
		    	ArrayList<PointF> list = new ArrayList<PointF>();
		    	Element curve = (Element)nodeList.item(i);
		    	Element ps = (Element)curve.getElementsByTagName("ps").item(0);
				NodeList pNodeList = ps.getElementsByTagName("p");
				for(int j=0;j<pNodeList.getLength();j++){
					String[] pStrings = pNodeList.item(j).getTextContent().split(",");
					PointF pointF = new PointF(Integer.parseInt(pStrings[0]), Integer.parseInt(pStrings[1]));
					list.add(pointF);
				}
				resultList.add(list);
			}
		}
		return resultList;
	}
	public void draw(){
		
	}
	public Element getRootElement() {
		return rootElement;
	}
	public boolean removeCurve(Point ptCenter,float scale){
		boolean match = false;
		if(rootElement==null){
			return false;
		}
		NodeList nodeList = rootElement.getElementsByTagName("curve");
		if(null!=nodeList && 0 < nodeList.getLength()){  
		    for(int i=0; i < nodeList.getLength(); i++){
		    	Element curve = (Element)nodeList.item(i);
		    	Element ps = (Element)curve.getElementsByTagName("ps").item(0);
				NodeList pNodeList = ps.getElementsByTagName("p");
				if(null==pNodeList||1>=pNodeList.getLength()){
					continue;
				}
				String[] pointsStrings = pNodeList.item(0).getTextContent().split(",");
				float x = Integer.parseInt(pointsStrings[0]);
				float y = Integer.parseInt(pointsStrings[1]);
				Point ptStart = new Point();
				Point ptEnd = new Point();
				ptStart.x = (int)(x * scale);
				ptStart.y = (int)(y * scale);
				for(int j=1;j<pNodeList.getLength();j++){
					String[] pStrings = pNodeList.item(j).getTextContent().split(",");
					x = Integer.parseInt(pStrings[0]) * scale;
					y = Integer.parseInt(pStrings[1]) * scale;
					ptEnd.x = (int)x;
					ptEnd.y = (int)y;
					if(CurveUtil.lineInterCircle(ptStart, ptEnd, ptCenter)){
						try{
							curve.getParentNode().removeChild(curve);
							match = true;
						}catch (Exception e){
						}
						//break;
					}
					ptStart.x = (int)x;
					ptStart.y = (int)y;
				}
		    }
		}
		return match;
	}
}
