package com.excellence.pdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * 正文批注
 * */
public class TextOpinion {
	private List<SignContent> signContents;
	private Document doc;
	private Element rootElement;
	private File file = null;
	public TextOpinion(File file)throws Exception{
		this.file = file;
		DocumentBuilderFactory domfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder dombuilder = domfac.newDocumentBuilder();
		InputStream is = new FileInputStream(file);
		doc = dombuilder.parse(is);
		rootElement = doc.getDocumentElement();
	}
	public boolean removeSign(){
		boolean modify = false;
		Element rootElement = this.doc.getDocumentElement();
		NodeList nodeList = rootElement.getElementsByTagName("s_sign_content");
		if(nodeList!=null){
			modify = true;
			for(int i=0; i < nodeList.getLength(); i++){
				rootElement.removeChild(nodeList.item(i));
			}
		}
		return modify;
	}
	public void save(){
		try {
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty("indent", "yes");
			transformer.transform(new DOMSource(this.doc), new StreamResult(new FileOutputStream(this.file)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public SignContent getSignContent(int page){
		Element signElement = null;
		try {
			NodeList nodeList = rootElement.getElementsByTagName("s_sign_content");
			if(null!=nodeList && 0 < nodeList.getLength()){
				for(int i=0; i < nodeList.getLength(); i++){
					signElement = (Element)nodeList.item(i);
					Element pageElement = (Element)signElement.getElementsByTagName("page").item(0);
					if(Integer.parseInt(pageElement.getTextContent())==page){
						break;
					}else{
						signElement = null;
					}
				}
			}
			return new SignContent(doc,signElement);
		} catch (Exception e) {
			return null;
		}
	}
}
