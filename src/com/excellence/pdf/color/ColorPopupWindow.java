
package com.excellence.pdf.color;


import com.artifex.mupdfdemo.R;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
/**
 * 调色板窗口
 * */
public class ColorPopupWindow{
	protected TextView fontTextView;
	protected SeekBar seekBar;
	protected ColorPickerView colorPickerView;
	protected PopupWindow popupWindow;
	private int size = 2;
	private int color = Color.BLACK;
	private OnColorChangedListener listener;
	public ColorPopupWindow(Context context,OnColorChangedListener listener){
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view =  layoutInflater.inflate(R.layout.draw_color, null);
		view.setBackgroundResource(R.drawable.rounded_corners_view);
		this.listener = listener;
		colorPickerView = (ColorPickerView)view.findViewById(R.id.colorPickerView);
		fontTextView = (TextView)view.findViewById(R.id.fontText);
		seekBar = (SeekBar)view.findViewById(R.id.fontSeekBar);
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				ColorPopupWindow.this.fontTextView.setText("笔画大小:"+(progress+1));
			}
		});
		popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT,  
	                ViewGroup.LayoutParams.WRAP_CONTENT);
		popupWindow.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.rounded_corners_pop)); 
		// 使其聚集
		//popupWindow.setFocusable(true);
		// 设置允许在外点击消失
		popupWindow.setOutsideTouchable(true);
		// 这个是为了点击“返回Back”也能使其消失
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		popupWindow.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss() {
				size = seekBar.getProgress() + 1;
				color = colorPickerView.getColor();
				ColorPopupWindow.this.listener.changed(color,size);
			}
		});
	}
	public void show(View parent){
		popupWindow.showAsDropDown(parent,0, 0);
	}
	public void hide(){
		if(popupWindow!=null){
			popupWindow.dismiss();
		}
	}
}
