package com.excellence.pdf.color;

import android.graphics.Color;

public class ColorUtil {

	public static int to16(int color){
		int r = Color.red(color);
		int g = Color.green(color);
		int b = Color.blue(color);
		int rgb = r << 16 | g << 8 | b;
		return rgb;
	}
	public static int toRGB(int color){
		 int red = (color & 0xff0000) >> 16; 
	     int green = (color & 0x00ff00) >> 8; 
	     int blue = (color & 0x0000ff);
	     return Color.rgb(red, green, blue);
	}
	public static int toColor(String colorString){
		try{
			return Color.parseColor("#"+colorString);
		}catch (Exception e) {
			return Color.BLACK;
		}
	}
	public static String toRgbString(int color){
		try{
			String r = Integer.toHexString(Color.red(color));
			String g = Integer.toHexString(Color.green(color));
			String b = Integer.toHexString(Color.blue(color));
			String rString = r.length()==1?"0"+r:r;
			String gString = g.length()==1?"0"+g:g;
			String bString = b.length()==1?"0"+b:b;
			return rString+gString+bString;
		}catch (Exception e) {
			return "000000";
		}
	}

}
