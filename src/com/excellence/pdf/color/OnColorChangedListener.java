package com.excellence.pdf.color;
/**
 * 调色板监听器,在ColorPopupWindow构造方法中使用。
 * */
public interface OnColorChangedListener {
	public void changed(int color,int size);
}
