package com.excellence.pdf.comment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.artifex.mupdfdemo.R;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
/**
 * 批示列表数据适配器
 * */
public class CommentAdapter extends BaseAdapter {  
    private LayoutInflater mInflater;
    private List<Map<String, Object>> mData = new ArrayList<Map<String,Object>>();
    public CommentAdapter(Context context,List<Map<String, Object>> mData) {  
        this.mInflater = LayoutInflater.from(context);
        this.mData = mData;
    }  
    public int getCount() {  
        return mData.size();  
    }  
    public Object getItem(int arg0) {  
        return mData.get(arg0);  
    }  
    public long getItemId(int arg0) {
        return arg0;  
    }  
    public View getView(int position, View convertView, ViewGroup parent) {  
        ViewHolder holder = null;  
        if (convertView == null) {  
            holder = new ViewHolder();  
            convertView = mInflater.inflate(R.layout.draw_comment_list, null);  
            holder.commentCheckBox = (CheckBox)convertView.findViewById(R.id.commentCheckBox);  
            holder.comentId = (TextView) convertView.findViewById(R.id.comentId);
            holder.commentEditable = (TextView) convertView.findViewById(R.id.commentEditable);
            holder.commentEditor = (TextView) convertView.findViewById(R.id.commentEditor);
            holder.commentActName = (TextView) convertView.findViewById(R.id.commentActName);
            holder.comentEditTime = (TextView) convertView.findViewById(R.id.comentEditTime);
            convertView.setTag(holder);           
        } else {  
            holder = (ViewHolder) convertView.getTag();  
        }
        holder.comentId.setText((String) mData.get(position).get("comentId"));
        String editable = (String) mData.get(position).get("commentEditable");
        holder.commentEditable.setText(editable);
        holder.commentEditor.setText((String) mData.get(position).get("commentEditor")); 
        holder.comentEditTime.setText((String) mData.get(position).get("comentEditTime")); 
        holder.commentActName.setText((String) mData.get(position).get("commentActName")); 
        if("1".equals((String)mData.get(position).get("check"))){
        	holder.commentCheckBox.setChecked(true);
        }else{
        	holder.commentCheckBox.setChecked(false);
        }
        return convertView;  
    }
    private final class ViewHolder {  
        public CheckBox commentCheckBox;  
        public TextView comentId;
        public TextView commentEditable;
        public TextView commentEditor;
        public TextView commentActName;
        public TextView comentEditTime;
    }
}
