package com.excellence.pdf.comment;

import java.util.List;
/**
 * 批示列表选取监听器
 * */
public interface OnCommentChangedListener {
	public void commentChanged(List<String> commentIds);
}
