package com.excellence.pdf.comment;

import com.excellence.pdf.TextOpinion;

/**
 * 批注类
 * */
public class PdfComment {
	private String id;
	private String author;
	private String editDate;
	private String actName;
	private String filePath;
	private boolean editable = false;
	private boolean check = true;
	private TextOpinion textOpinion;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getEditDate() {
		return editDate;
	}
	public void setEditDate(String editDate) {
		this.editDate = editDate;
	}
	public String getActName() {
		return actName;
	}
	public void setActName(String actName) {
		this.actName = actName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public boolean isEditable() {
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	public boolean isCheck() {
		return check;
	}
	public void setCheck(boolean check) {
		this.check = check;
	}
	public TextOpinion getTextOpinion() {
		return textOpinion;
	}
	public void setTextOpinion(TextOpinion textOpinion) {
		this.textOpinion = textOpinion;
	}
}
