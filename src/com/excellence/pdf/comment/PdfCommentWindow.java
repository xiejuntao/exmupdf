package com.excellence.pdf.comment;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.artifex.mupdfdemo.R;
import com.excellence.pdf.util.StringUtil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

/**
 * 正文的批示列表窗口
 * */
public class PdfCommentWindow{
	protected PopupWindow popupWindow;
	private OnCommentChangedListener listener;
	private List<PdfComment> comments;
	private List<String> comentIds = new ArrayList<String>();
	CommentAdapter commentAdapter;
	List<Map<String, Object>> data;
	public PdfCommentWindow(Context context,List<PdfComment> comments,OnCommentChangedListener listener) {
		super();
		this.listener = listener;
		this.comments = comments;
		for(PdfComment pdfComment:this.comments){
			if(!StringUtil.notEmpty(pdfComment.getId())){
				this.comments.remove(pdfComment);
			}
		}
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view =  layoutInflater.inflate(R.layout.draw_comment,null);
		ListView listView = (ListView)view.findViewById(R.id.commentList);
		TextView commentTextView = (TextView)view.findViewById(R.id.commentText);
		data = getData();
		commentTextView.setText("批注列表("+data.size()+")");
		commentAdapter = new CommentAdapter(context,data);
		listView.setAdapter(commentAdapter);
		Button selButton = (Button)view.findViewById(R.id.selButton);
		selButton.setText("全不选");
		selButton.setTextColor(Color.WHITE);
		selButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Button tButton = (Button)v;
				boolean isAllSelected = "全选".equals(tButton.getText().toString());//为真时，不全选
				for(Map<String, Object>  map:data){
					String isEditable = (String)map.get("commentEditable");
					if("1".equals(isEditable)){
						continue;
					}
					String commentId = (String)map.get("comentId");
					if(isAllSelected){
						if(!comentIds.contains(commentId)){
							comentIds.add(commentId);
						}
						map.put("check", "1");
						tButton.setText("全不选");
					}else{
						if(comentIds.contains(commentId)){
							comentIds.remove(commentId);
						}
						map.put("check", "0");
						tButton.setText("全选");
					}
				}
				commentAdapter.notifyDataSetChanged();
				PdfCommentWindow.this.listener.commentChanged(comentIds);
			}
		});
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int arg2,
					long arg3) {
				CheckBox cb = (CheckBox) view.findViewById(R.id.commentCheckBox);
				TextView idTextView = (TextView)view.findViewById(R.id.comentId);
				String commentId = (String)idTextView.getText();
				TextView editableTextView= (TextView)view.findViewById(R.id.commentEditable);
				String isEditable = (String)editableTextView.getText();
				if("0".equals(isEditable)){
					Map<String, Object>  map = data.get(arg2);
					if(cb.isChecked()){
						map.put("check", "0");
						cb.setChecked(false);
						comentIds.remove(commentId);
					}else{
						map.put("check", "1");
						cb.setChecked(true);
						comentIds.add(commentId);
					}
				}
				PdfCommentWindow.this.listener.commentChanged(comentIds);
			}
		});
		popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT,  
                ViewGroup.LayoutParams.WRAP_CONTENT,true);
		// 设置允许在外点击消失
		popupWindow.setOutsideTouchable(true);
		// 这个是为了点击“返回Back”也能使其消失
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		//popupWindow.setWidth(340);
	}
	public void show(View parent){
		popupWindow.showAsDropDown(parent,0, 0);
	}
	private List<Map<String, Object>> getData() {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for(PdfComment comment:comments){
	        Map<String, Object> map = new HashMap<String, Object>();
	        map.put("comentId", comment.getId());
	        map.put("commentEditor",comment.getAuthor());
	        map.put("comentEditTime",comment.getEditDate());
	        map.put("commentActName",comment.getActName());
	        map.put("check", "1");
	        if(comment.isEditable()){
	        	map.put("commentEditable", "1");
	        }else{
	        	map.put("commentEditable", "0");
	        }
	        comentIds.add(comment.getId());
	        list.add(map);
        }
        return list;
    }
}
