package com.excellence.pdf.net.codec;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

public class Decoder extends CumulativeProtocolDecoder {

    protected boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) throws Exception {
    	try{

    		MessageParser parser=null;
     	   if(session.getAttribute("parser")==null){
     		   parser=new MessageParser();
     		   session.setAttribute("parser", parser);
     	   }else{
     		   parser=(MessageParser)session.getAttribute("parser");
     	   }
     	   parser.read(in);
     	   if(parser.hasMsg()){
     		   for(String msg:parser.getMsgs()){
     			   out.write(msg);
     		   }
     	   }
     	   parser.clearMsgs();//清除已经处理的消息
     	   return !in.hasRemaining();
        }
        catch(Exception ex){
     	   System.out.println(ex.getMessage());
     	   return false;
        }
    }
}
