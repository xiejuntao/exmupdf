package com.excellence.pdf.net.codec;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

public class Encoder implements ProtocolEncoder {
    public void encode(IoSession session, Object message, ProtocolEncoderOutput out) throws Exception {
    	CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();
        String value = (message == null ? "" : message.toString());
        if(value.trim().equals(""))
        	return;
        value=value.replace("<", "&lt;").replace(">", "&gt;");
        value="<begin>"+value+"<end>";
        IoBuffer buf = IoBuffer.allocate(value.getBytes().length).setAutoExpand(true);
        buf.putString(value, encoder);
        buf.flip();
        out.write(buf);
    }

    public void dispose(IoSession session) throws Exception {

    }
}