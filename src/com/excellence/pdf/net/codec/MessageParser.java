package com.excellence.pdf.net.codec;

import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;

import org.apache.mina.core.buffer.IoBuffer;

/**
 * 消息分析器。根据消息开始和结束标识，从接收数据流中获取消息。
 * @author shenx
 *
 */
public class MessageParser {
	/**
	 * 消息缓冲区
	 */
	protected StringBuilder buffer = new StringBuilder();
	/**
	 * 待处理消息列表
	 */
	protected ArrayList<String> msgs = new ArrayList<String>();
//	protected ArrayList<String> jsonMarks = new ArrayList<String>();
	
	/**
	 * 消息开始标识
	 */
	protected final String beginTag = "<begin>";
	/**
	 * 消息结束标识
	 */
	protected final String endTag = "<end>";
	/**
	 * 结束标识的长度
	 */
	protected final int endTagLength = 5;
	int readCount = 0;
	CharsetDecoder decoder;

	public MessageParser() {
		decoder = Charset.forName("UTF-8").newDecoder()
				.onMalformedInput(CodingErrorAction.REPLACE)
				.onUnmappableCharacter(CodingErrorAction.REPLACE);
	}

	/**
	 * 从ioBuffer中读取消息
	 * 接收到消息后，将新接收到的消息放到消息缓冲区，然后根据消息开始和结束标识获取消息。
	 * @param ioBuffer
	 */
	public void read(IoBuffer ioBuffer) {
		readCount++;
		CharBuffer charBuffer = CharBuffer.allocate(ioBuffer.capacity());
		decoder.reset();
		decoder.decode(ioBuffer.buf(), charBuffer, false);
		char[] buf = new char[charBuffer.position()];
		charBuffer.flip();
		charBuffer.get(buf);
		int readCharCount = buf.length;
		if (readCharCount == 0) {
			return;
		}
		buffer.append(buf);
		buffer.trimToSize();
		int msgStartIndex = buffer.indexOf(beginTag);
		int msgEndIndex = buffer.indexOf(endTag);
		while (msgStartIndex >= 0 && msgEndIndex > 0) {
			if (foundMsg(msgStartIndex, msgEndIndex)) {
				msgStartIndex = buffer.indexOf(beginTag);
				msgEndIndex = buffer.indexOf(endTag);
			} else {
				break;
			}
		}
	}

	/**
	 * 根据消息开始标识位置和结束标签位置获取待处理消息
	 * 并将获取到的消息的内容从buffer中移除
	 * @param startIndex 开始标识位置
	 * @param endIndex 结束标识位置
	 * @return true，成功获取一条消息；false，标识位置不合法，未能获取消息。
	 */
	protected boolean foundMsg(int startIndex, int endIndex) {
		if (startIndex >= 0 && endIndex > 0) {
			if (startIndex < endIndex) {
				String msg = buffer.substring(startIndex, endIndex
						+ endTagLength);
				msg=msg.replace("<begin>", "").replace("<end>", "");
				msg=msg.replace("&lt;","<").replace("&gt;",">");
				msgs.add(msg);
			}
			String temp = buffer.substring(endIndex + endTagLength);
			buffer.delete(0, buffer.length());
			buffer.append(temp);
			buffer.trimToSize();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 是否有待处理的消息
	 * @return
	 */
	public boolean hasMsg() {
		return msgs.size() > 0;
	}

	/**
	 * 获取待处理的消息
	 * @return
	 */
	public ArrayList<String> getMsgs() {
		return msgs;
	}
	
	/**
	 * 清除待处理消息
	 */
	public void clearMsgs(){
		msgs.clear();
	}
}

