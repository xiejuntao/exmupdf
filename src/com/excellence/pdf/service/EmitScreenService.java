package com.excellence.pdf.service;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

import org.apache.mina.transport.socket.nio.NioSocketConnector;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.artifex.mupdfdemo.MuPDFReaderView;
import com.excellence.pdf.net.PdfClientSupport;
import com.excellence.pdf.net.PdfIoHandlerAdapter;
import com.excellence.pdf.net.PdfIoHandlerAdapter.NetCallback;

public class EmitScreenService extends Service implements Callback{
	//private ScheduledExecutorService scheduler = null;
	private int tempPage = 0;
	private NioSocketConnector socketConnector;
	private PdfClientSupport client;
	private PdfIoHandlerAdapter ioHandlerAdapter;
	private final String HOST = "10.1.4.255";
	private final int PORT = 9099;
	private final IBinder mBinder = new LocalBinder();
	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}
	@Override
	public void onCreate() {
Log.d("EmitScreenService","onCreate");
		super.onCreate();
		Handler handler = new Handler(this);
		handler.post(new Runnable() {
			@Override
			public void run() {
				socketConnector = new NioSocketConnector();
				SocketAddress address = new InetSocketAddress(HOST,PORT);
				ioHandlerAdapter = new PdfIoHandlerAdapter(new NetCallback(){
					@Override
					public void connected() {
						Log.d("EmitScreenService","已连接");
					}
					@Override
					public void loggedIn() {
						// TODO Auto-generated method stub
						
					}
					@Override
					public void loggedOut() {
						// TODO Auto-generated method stub
						
					}
					@Override
					public void disconnected() {
						Log.d("EmitScreenService","已断开连接");
					}
					@Override
					public void messageReceived(String message) {
						Log.d("EmitScreenService","收到的信息:"+message);
					}
					@Override
					public void error(String message) {
						Log.d("EmitScreenService","错误:"+message);
					}
				});
		        client = new PdfClientSupport(ioHandlerAdapter);
		        boolean isConnected = client.connect(socketConnector,address);
		        Log.d("EmitScreenService","isConnected:"+isConnected);
			}
		});
	}
	@Override
	public void onDestroy() {
Log.d("EmitScreenService","onDestroy");
		super.onDestroy();
		client.quit();
	}

	public class LocalBinder extends Binder {
		public EmitScreenService getService() {
            return EmitScreenService.this;
        }
    }
	public void emitScreen(final MuPDFReaderView readerView){
Log.d("EmitScreenService","emitScreen");
		client.broadcast("{\"type\":\"ping\"}");
/*		Runnable task = new Runnable() {  
		    public void run() {
		    	int currentPage = readerView.getCurrentPage();
		    	if(currentPage!=tempPage){
		    		tempPage = currentPage;
		    		Log.d("EmitScreenService","run:"+currentPage);
		    	}
		    }
		};*/
		//scheduler.scheduleWithFixedDelay(task,0,1000, TimeUnit.MILLISECONDS); 
	}
	@Override
	public boolean handleMessage(Message arg0) {
		// TODO Auto-generated method stub
		return false;
	}
}
