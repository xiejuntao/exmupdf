/**
 * Copyright © 2013 EXCELLENCE INFORMATION, All Rights Reserved. 
 * 
 * http://excellence.com.cn
 * 
 */
package com.excellence.pdf.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.UUID;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

/**
 * 
 * 获取设备唯一标识，注意，是使用{@link #getDeviceUniqueId(Context)}方法。该方法会组合
 * 多种读取android唯一标识的方式，因为较难找到一种通用的、适用所有android设备的唯一标识读取方式。
 * 
 * 注意：{@link #getDeviceUniqueId(Context)}方法只会在第一次调用时生成唯一标识，该标识会保存到
 * SharedPreference和sdcard中。当 再次调用时，会先从SharedPreference中读取，读取不到时，
 * 再从sdcard读取，如果仍读取不到，才会重新生成唯一标识。
 * 
 * @author JQ.W
 * @version 1.0, 2013/3/7
 * @since 1.0
 */
public class DeviceIdUtil {
	
	/**
	 * 对组合标识串进行格式化（MD5），形成唯一标识。
	 * 注意：该方法只会在第一次调用时生成唯一标识，该标识会保存到
     * SharedPreference和sdcard中。当 再次调用时，会先从SharedPreference中读取，读取不到时，
 	 * 再从sdcard读取，如果仍读取不到，才会重新生成唯一标识。
	 */
	public static String getDeviceUniqueId(Context context) {
		String uniqueId = getDeviceUniqueId_SP(context);
		
		if (!TextUtils.isEmpty(uniqueId)) {
			Log.d(TAG, "get device unique id from Sharedpreference.");
			return uniqueId;
		}
		
		uniqueId = getDeviceUniqueId_Sdcard();
		if (!TextUtils.isEmpty(uniqueId)) {
			Log.d(TAG, "get device unique id from Sdcard.");
			
			// save it to sp
			saveDeviceUniqueId_SP(context, uniqueId);
			Log.d(TAG, "save device unique id to SP");
			return uniqueId;
		}
		
		// 只会在第一次调用时创建，
		String result = getCompoundUniqueIds(context);
		Log.d(TAG, "CREATE device unique id for the first time.");
		
		//“unknow”的情况 ： getSearialNum()为"unknow"，其它几个标识全为空
		if ("".equals(result) || "unknow".equals(result)) { 
			result = String.valueOf(UUID.randomUUID());
		}

		try {
			result = MD5.MD5Encode(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// save it to sp
		saveDeviceUniqueId_SP(context, result);
		Log.d(TAG, "save device unique id to SP");
		
		// save it to sdcard
		saveDeviceUniqueId_Sdcard(result);
		Log.d(TAG, "save device unique id to Sdcard.");
		return result;
	}
	
	/**
	 * androidId的获取: Settings.Secure.ANDROID_ID 是一串64位的编码（十六进制的字符串）， 
	 * 是随机生成的设备的第一个引导，其记录着一个固定值，通过它可以知道设备的寿命（在设备
	 * 恢复出厂设置后，该值可能会改变）。 ANDROID_ID也可视为作为唯一设备标识号的一个好选择。
	 * 对于Android 2.2（“Froyo”）之前的设备不是100％的可靠
	 */
	public static String getAndroidId(Context context) {
		String result = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
		return TextUtils.isEmpty(result) ? "" : result;
	}
	
	/**
	 * 获取设备的diviceId,由于各厂家的产品标准不统一，
	 * 可能存在相同的diviceId，也可能无法获取到diviceId
	 */
	public static String getDeviceId(Context context) {
		TelephonyManager telephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String result = telephonyMgr.getDeviceId();
		return TextUtils.isEmpty(result) ? "" : result;
	}
	
	/**
	 * 获取手机唯一标识IMSI</p> IMSI是国际移动用户识别码，相当于是手机的身份证号码。
	 * 比如说丢了手机可以把IMSI提供给商家进行地位追踪进而找回手机。
	 * 在拨号键盘里输入*＃06＃就可以看到了。<br>
	 * 
	 * 注：仅适用于手机，联想乐pad测试不通过！
	 */
	public static String getSubscriberId(Context context) {
		TelephonyManager telephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String result = telephonyMgr.getSubscriberId();
		return TextUtils.isEmpty(result) ? "" : result;
	}
	
	/**
	 * 序列号的获取:
	 * 从Android 2.3（“姜饼”）开始，通过android.os.Build.SERIAL方法序列号可被使用。
	 * 没有电话功能的设备也都需要上给出唯一的设备ID;  某些手机也可以需要这样做。
	 * 序列号可以用于识别MID（移动互联网设备）或PMP（便携式媒体播放器），
	 * 这两种设备都没有电话功能。通过读取系统属性值“ro.serialno”的方法
	 * ，可以使用序列号作为设备ID 。
	 * 序列号无法在所有Android设备上使用。(如：非正规设备)
	 */
	public static String getSearialNum(Context context) {
		String serialnum = "";
		try {
			Class<?> c = Class.forName("android.os.SystemProperties");
			Method get = c.getMethod("get", String.class, String.class);
			serialnum = (String) (get.invoke(c, "ro.serialno", "unknown"));
		} catch (Exception ignored) {
		}
		return TextUtils.isEmpty(serialnum) ? "" : serialnum;
	}
	
	
	/**
	 * 某一标识针对某一android设备可能获取不到，所以将多个进行组合，形成一个串。
	 */
	public static String getCompoundUniqueIds(Context context) {
		String result = DeviceIdUtil.getDeviceId(context) 
				/*+ ":"*/ + DeviceIdUtil.getAndroidId(context) 
				/*+ ":"*/ + DeviceIdUtil.getSubscriberId(context)
				/*+ ":"*/ + DeviceIdUtil.getSearialNum(context);
		return result;
	}
	
	
	private static String getDeviceUniqueId_SP(Context context) {
		SharedPreferences pres = context.getSharedPreferences(SP_FILE, //
				Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
		String value = pres.getString(SP_KEY, "");
		return value;
	}
	
	private static void saveDeviceUniqueId_SP(Context context, String deviceUniqueId) {
		SharedPreferences pres = context.getSharedPreferences(SP_FILE,//
				Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
		Editor editor = pres.edit();
		editor.putString(SP_KEY, deviceUniqueId);
		editor.commit();
	}
	
	private static String getDeviceUniqueId_Sdcard() {
		String result = "";
		try {
			String absFilePath = Environment.getExternalStorageDirectory() + "/" + FILE_FOLDER + "/" + FILE_NAME;
			File file = new File(absFilePath);
			InputStream inSream = new FileInputStream(file);
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int len = -1;
			while ((len = inSream.read(buffer)) != -1) {
				outStream.write(buffer, 0, len);
			}
			byte[] data = outStream.toByteArray();
			result = new String(data, "UTF_8");
			outStream.close();
			inSream.close();
		} catch (Exception e) {
			Log.i(TAG, "getDeviceUniqueId_Sdcard error");
		}
		return result;
	}

	private static void saveDeviceUniqueId_Sdcard(String deviceUniqueId) {
		FileOutputStream fos = null;
		try {
			String absFilePath = Environment.getExternalStorageDirectory() + "/" + FILE_FOLDER;
			File file = new File(absFilePath);
			if (!file.exists()) { // 文件夹不存在则创建
				file.mkdirs();
			}
			absFilePath = absFilePath + "/" + FILE_NAME;

			file = new File(absFilePath);
			if (!file.exists()) { // 文件不存在则创建
				file.createNewFile();
			}

			fos = new FileOutputStream(file, false); // false 不会追加
			fos.write(deviceUniqueId.getBytes("UTF_8"));
			fos.close();
		} catch (Exception e) {
			Log.i(TAG, "saveDeviceUniqueId_Sdcard error");
		} finally {
			if (fos != null) {
				try {
					fos.close();
					fos = null;
				} catch (IOException e) {
				}
			}
		}
	}
	
	private static final String SP_FILE = "sp_device_id";
	private static final String SP_KEY = "key_device_unique_id";
	private static final String FILE_FOLDER = "android_id";
	private static final String FILE_NAME = "device_id.txt";
	private static final String TAG = "device";
}
