/**
 * Copyright © 2012 EXCELLENCE INFORMATION, All Rights Reserved.
 * http://www.excellence.com.cn
 * 
 */
package com.excellence.pdf.util;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;

/**
 * 字符串操作类
 * 
 * @since 1.0
 * @author Jinquan.Wang
 * @date 2012/12/20
 */
public class StringUtil {
	
	/** 截掉字符串后7位 */
	public static String trimSuffix7Char(String timeString) {
		if (!TextUtils.isEmpty(timeString) && timeString.length() > 7) {
			return timeString.substring(0, timeString.length() - 7);
		}
		return timeString;
	}
	
	public static String getStrWithColor(int content) {
		return getStrWithColor(String.valueOf(content));
	}

	public static String getStrWithColor(String content) {
		return "<font color=#E27507>" + content + "</font>";
	}
	/**
	 * 拆分形如"/myfolder/3/4/"的目录
	 * 
	 * @param dirSection 要拆分的目录
	 * @return String[]
	 */
	public static String[] splitDirString(String dirSection) {
		if (dirSection == null || "".equals(dirSection.trim()))
			return null;
		dirSection = dirSection.trim();
		if ('/' == (dirSection.charAt(0))) {
			dirSection = dirSection.substring(1);
		}
		if (dirSection != null && !"".equals(dirSection)) {
			if ('/' == (dirSection.charAt(dirSection.length() - 1))) {
				dirSection = dirSection.substring(0, dirSection.length() - 1);
			}
		}
		if (dirSection != null && !"".equals(dirSection)) {
			String[] dirs = dirSection.split("/");
			return dirs;
		}
		return null;
	}
	public static String handleUserInputUrl(String userInputUrl) {
		if (TextUtils.isEmpty(userInputUrl))
			return "";

		if (!userInputUrl.contains("/masp/services")) {
			if (userInputUrl.endsWith("/")) {
				userInputUrl += "masp/services";
			} else {
				userInputUrl += "/masp/services";
			}
		}
		return userInputUrl;
	}
	
	public static String httpsToHttp(String url) {
		if (TextUtils.isEmpty(url))
			return "";
		return url.replace("https://", "http://");
	}
	
	public static Spanned getHtmlStr(String str, String color) {
		str = replaceEscapeCharacter(str);
		return Html.fromHtml("<font color="+ color +">" + str + "</font>");
	}
	
	public static String replaceEscapeCharacter(String srcString) {
		if(TextUtils.isEmpty(srcString))
			return srcString;
		
		srcString = srcString.replace("&", "&amp;");
		srcString = srcString.replace("<", "&lt;");
		srcString = srcString.replace(">", "&gt;");
		srcString = srcString.replace("\'", "&apos;");//单引号
		srcString = srcString.replace("\"", "&quot;");//双引号
		return srcString;
	}
	
	/**
	 * 替换字符串中特殊字符
	 */
	public static String encodeString(String srcString) {
		if(TextUtils.isEmpty(srcString))
			return srcString;
		
		srcString = srcString.replace("&amp;", "&");
		srcString = srcString.replace("&lt;", "<");
		srcString = srcString.replace("&gt;", ">");
		srcString = srcString.replace("&apos;", "\'");//单引号
		srcString = srcString.replace("&quot;", "\"");//双引号
		return srcString;
	}

	
	public static boolean strToBoolean(String val) {
		if (TextUtils.isEmpty(val))
			return false;

		if ("true".equals(val.toLowerCase()) || "1".equals(val))
			return true;

		return false;
	}
	
	/**
	 * 传入"/mnt/sdcard/exoa/test.apk" 返回“test.apk”
	 * 
	 * @param absFilePath sdcard 完整路径，例：“/mnt/sdcard/exoa/test.apk”
	 */
	public static String getFileName(String absFilePath) {
		if (TextUtils.isEmpty(absFilePath))
			return UUID.randomUUID().toString() + ".txt";

		String fileName = null;
		try {
			fileName = absFilePath.substring(absFilePath.lastIndexOf("/") + 1, absFilePath.length());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileName;
	}
	/**
	 * 判断输入的字符串是否是非零的正整数
	 * */
	public static boolean isNum(String str){
		Pattern pattern = Pattern.compile("^[1-9]+\\d*$");
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}
	
	/**
	 * 将2012-02-12 12:00转换为2012年2月12日 12点00分
	 */
	public static String formatTime(String originalStr) {
		if (TextUtils.isEmpty(originalStr)) 
			return "";
		
		originalStr = originalStr.replace(":", "点");
		originalStr = originalStr.replaceFirst("-", "年");
		originalStr = originalStr.replaceFirst("年0", "年");
		originalStr = originalStr.replaceFirst("-", "月");
		originalStr = originalStr.replaceFirst("月0", "月");
		originalStr = originalStr.replaceFirst(" ", "日 ");
		originalStr = originalStr + "分";
		return originalStr;
	}
	public static boolean notEmpty(String value) {
		if(value!=null&&!"".equals(value)){
			return true;
		}
		return false;
	}
	/**
	 * 判断输入的字符串是否是正整数
	 * */
	public static boolean isInt(String str){
		Pattern pattern = Pattern.compile("^[-+]?[d]*$"); 
		return pattern.matcher(str).matches(); 
	}
	/**
	 *  判断输入的字符串是否是浮点数
	 * */
	public static boolean isFloat(String str){
		Pattern pattern = Pattern.compile("^[-\\+]?[.\\d]*$");    
	    return pattern.matcher(str).matches();
	}
}
